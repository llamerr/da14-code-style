"use strict";

function foo() {
    return;
}

var bar = function() {
    return;
};

foo();
bar();


function add(x, y) {
// --->..return x + y;

    return x + y;    /*error Mixed spaces and tabs.*/

    switch(a){
        case "a":
            break;
        case "b":
            break;
    }
}

function main() {
// --->var x = 5,
// --->....y = 7;

    var x = 5,
        y = 7;         /*error Mixed spaces and tabs.*/
}

var name = "ESLint"          /*error Missing semicolon.*/

function foo() {
    return true;;
    console.log("done");      /*error Found unexpected statement after a return.*/
}

var foo = { "bar": "This is a bar.", "baz": { "qux": "This is a qux" }, "difficultsdfsdf": "to read", "difficult": "to read" }; /*error Line 3 exceeds the maximum line length of 80.*/

//max function length... kinda
function add(x, y) {
    "use strict";
    var a1;
    var a2;
    var a3;
    var a4;
    var a5;
    var a6;
    var a7;
    var a8;
    var a9;
    var b1;
    var b2;
    var b3;
    var b4;
    var b5;
    var b6;
    var b7;
    var b8;
    var b9;
    var c1;
    var c2;
    var c3;
    var c4;
    var c5;
    var c6;
    var c7;
    var c8;
    var c9;
    var d1;
    var d2;
    var d3;
    var d4;
    var d5;
    var d6;
    var d7;
    var d8;
    var d9;
    switch(a){
        case "a":
            break;
        case "b":
            break;
    }
}


Foo.prototype.bar = function() {}; /*error Missing function expression name.*/

(function() {                      /*error Missing function expression name.*/
    // ...
}())

function foo (bar, baz, qux, qxx, snafu) { /*error This function has too many parameters (4). Maximum allowed is 3.*/
    doSomething();
}


var double = "double";                                 /*error Strings must use singlequote.*/
var unescaped = "a string containing 'single' quotes"; /*error Strings must use singlequote.*/
var foo;
if (foo) foo++; /*error Expected { after 'if' condition.*/

while (bar)     /*error Expected { after 'while' condition.*/
    baz();

if (foo) {      /*error Expected { after 'else'.*/
    baz();
} else qux();


var my_favorite_color = '#112C85'; /*error Identifier 'my_favorite_color' is not in camel case.*/

function do_something() {          /*error Identifier 'do_something' is not in camel case.*/
    'use strict';
    // ...
}

obj.do_something = function some() {    /*error Identifier 'do_something' is not in camel case.*/
    'use strict';
    // ...
};

var obj = {
    my_pref: 1                     /*error Identifier 'my_pref' is not in camel case.*/
};


function foo() {
    'use strict';
    var bar,          /*error Split 'var' declarations into multiple statements.*/
        baz,
        qix = 1;
}

function foo() {
    'use strict';
    var bar,          /*error Split 'var' declarations into multiple statements.*/
        qux;

    if (baz) {
        qux = true;
    }
}


var object = {
  "a": 0,    /*error Unnecessarily quoted property `a` found.*/
  "0": 0,    /*error Unnecessarily quoted property `0` found.*/
  "true": 0, /*error Unnecessarily quoted property `true` found.*/
  "null": 0  /*error Unnecessarily quoted property `null` found.*/
};

fooFunction(params, function(err, result){
  //doSomething
});


// these are the same as "10"
var a = '1' + '0';         /*error Unexpected string concatenation of literals.*/


function foo (bar,baz, qux,qxx, snafu){
  if(something) { doSomething(); }
}
