function foo() {
  'use strict';
  return;
}

var bar = function bar() {
  'use strict';
  return;
};

function add(x, y) {
  'use strict';

  switch (a) {
    case 'a':
      break;
    case 'b':
      break;
  }

  return x + y;
}

var foo = {bar: 'This is a bar.', baz: {qux: 'This is a qux'}, baz: {qux: 'Thqux'}, difficult: 'to read and write'}; //not an error not an error not an error

Foo.prototype.bar = function bar() {
  'use strict';
};

(function bar() {
  'use strict';
  // ...
}());


var single = 'single';
/*error Strings must use doublequote.*/
var unescaped = 'a string containing "double" quotes';
/*error Strings must use doublequote.*/

if (foo) {
  foo++;
}

while (bar) {
  baz();
}

if (foo) {
  baz();
} else {
  qux();
}


var myFavoriteColor = '#112C85';
var _myFavoriteColor = '#112C85';
var myFavoriteColor_ = '#112C85';
var MY_FAVORITE_COLOR = '#112C85';
var foo = bar.baz_boom;
var foo = {qux: bar.baz_boom};

obj.do_something();


function foo() {
  'use strict';
  var bar;
  var baz;
}

function foo() {
  'use strict';
  var bar;

  if (baz) {
    var qux = true;
  }
}


var object1 = {
  'a-b': 0,
  '0x0': 0,
  '1e2': 0
};

var object2 = {
  foo: 'bar',
  baz: 42,
  true: 0,
  0: 0,
  'qux-lorem': true
};
